# mytwilliotest

Utiliser twillio Trial

# Intall twillio


`pip install twilio`

# Add code to your project

```bash
from twilio.rest import Client


account_sid = 'twillio_account_id'
auth_token = 'twillio_token'
client = Client(account_sid, auth_token)

message = client.messages \
                .create(
                    body="votre message ici",
                    from_='twillionuber',
                    to='+22548278174'
                )

# thank you
```